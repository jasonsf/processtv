#!/bin/bash
if [ $# -lt 1 ]
then
    echo "Usage: `basename $0` <filename> (must be a mkv file)"
    exit 1
fi

#vars
file=$(basename "$1")
scriptDir="$(dirname "${BASH_SOURCE[0]}")"
logfile="$scriptDir/plexPost.log"
ondeck="$scriptDir/ondeck"
#ondecktmp="$scriptDir/ondecktmp"
echo $(date) "Adding '$file' to ondeck list..." >> $logfile
#if [ -f "$ondeck" ]
#then
#	mv $ondeck $ondecktmp
#fi
echo $file >> $ondeck
awk '!seen[$0]++' $ondeck | sponge $ondeck
echo $(date) "...Done!" >> $logfile
